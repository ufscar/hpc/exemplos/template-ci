#include <cuda_runtime.h>
#include <device_launch_parameters.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#define BLOCK_SIZE 1024

#define CHECK_CUDA_ERROR(val) check((val), #val, __FILE__, __LINE__)
template <typename T>
void check(T err, const char* const func, const char* const file,
           const int line)
{
    if (err != cudaSuccess)
    {
        std::cerr << "CUDA Runtime Error at: " << file << ":" << line
                  << std::endl;
        std::cerr << cudaGetErrorString(err) << " " << func << std::endl;
        // We don't exit when we encounter CUDA errors in this example.
        // std::exit(EXIT_FAILURE);
    }
}

#define CHECK_LAST_CUDA_ERROR() checkLast(__FILE__, __LINE__)
void checkLast(const char* const file, const int line)
{
    cudaError_t err{cudaGetLastError()};
    if (err != cudaSuccess)
    {
        std::cerr << "CUDA Runtime Error at: " << file << ":" << line
                  << std::endl;
        std::cerr << cudaGetErrorString(err) << std::endl;
        // We don't exit when we encounter CUDA errors in this example.
        // std::exit(EXIT_FAILURE);
    }
}

__global__ void addKernel(int* c, const int* a, const int* b, int size) {
    int row = blockIdx.y * blockDim.y + threadIdx.y;
    int col = blockIdx.x * blockDim.x + threadIdx.x;
    int i = row * size + col;
    if (i < size)
        c[i] = a[i] + b[i];
}

// Helper function for using CUDA to add vectors in parallel.
void addWithCuda(int* c, const int* a, const int* b, int size) {
    int* dev_a = nullptr;
    int* dev_b = nullptr;
    int* dev_c = nullptr;

    // Aloca memória na GPU
    CHECK_CUDA_ERROR(cudaMalloc((void**)&dev_c, size * sizeof(int)));
    CHECK_CUDA_ERROR(cudaMalloc((void**)&dev_a, size * sizeof(int)));
    CHECK_CUDA_ERROR(cudaMalloc((void**)&dev_b, size * sizeof(int)));

    // Copia input para GPU
    CHECK_CUDA_ERROR(cudaMemcpy(dev_a, a, size * sizeof(int), cudaMemcpyHostToDevice));
    CHECK_CUDA_ERROR(cudaMemcpy(dev_b, b, size * sizeof(int), cudaMemcpyHostToDevice));

    addKernel<<<(size + BLOCK_SIZE - 1) / BLOCK_SIZE, BLOCK_SIZE>>>(dev_c, dev_a, dev_b, size);

    // cudaDeviceSynchronize espera terminar e retorna eventuais erros
    CHECK_CUDA_ERROR(cudaDeviceSynchronize());

    // Copia o resultado
    CHECK_CUDA_ERROR(cudaMemcpy(c, dev_c, size * sizeof(int), cudaMemcpyDeviceToHost));

    // Libera a memória na GPU
    CHECK_CUDA_ERROR(cudaFree(dev_c));
    CHECK_CUDA_ERROR(cudaFree(dev_a));
    CHECK_CUDA_ERROR(cudaFree(dev_b));
}

int main() {
    printf("Programa GPU inciando...\n");
    float t;
    int size, sizeB;
    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    int nA, nB;
    int *A, *B, *C;
    int i;
    FILE *fC;
    FILE *fA = fopen("A", "r");
    FILE *fB = fopen("B", "r");
    if(fscanf(fA, " %d", &nA) < 0) exit(1);
    if(fscanf(fB, " %d", &nB) < 0) exit(1);
    if(nA != nB) {
        printf("ERRO: matrizes geradas incorretamente\n");
        exit(1);
    }

    size = nA*nA;
    sizeB = size*sizeof(int);

    // inicializa as matrizes A e B
    A = (int*) malloc(sizeB);
    B = (int*) malloc(sizeB);
    C = (int*) malloc(sizeB);
    for(i=0; i<size; i++) {
        if(fscanf(fA, " %d", A+i) < 0) exit(1);
        if(fscanf(fB, " %d", B+i) < 0) exit(1);
    }
    fclose(fB);
    fclose(fA);

    cudaEventRecord(start, 0);
    addWithCuda(C, A, B, nA*nA);
    cudaEventRecord(stop, 0);
    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&t, start, stop);
    t /= 1000;

    // Imprime a matriz resultado
    fC = fopen("C_gpu", "w");
    fprintf(fC, "%d", nA);
    for(i=0; i<nA*nA; i++)
        fprintf(fC, " %d", C[i]);
    fprintf(fC, " %lf", t);
    fclose(fC);

    // Libera memória na CPU
    free(A);
    free(B);
    free(C);

    cudaDeviceReset();

    return 0;
}
